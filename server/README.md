# Social media api project

# USED

- Express
- MongoDB
- Mongoose
- Bcrypt
- JWT
- Lodash

# SETUP

```shell
git clone https://github.com/sanjarbek-ismatov/social-media-api.git
cd social-media-api
npm install
npm run dev

```
