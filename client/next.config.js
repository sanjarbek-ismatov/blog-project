/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  images: {
    domains: ["wallpaperset.com"],
  },

 
};

module.exports = nextConfig;

